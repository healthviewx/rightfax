__author__ = 'murugesan.p'

"""
Setuptools for RightFax Webservices

"""

from setuptools import setup, find_packages


setup(
    name='rightfax',
    description='RightFax Webservices',
    version='0.0.1',
    packages=find_packages(),
    zip_safe=False,
    url='https://gitlab.com/healthviewx/rightfax.git',
    author='Murugesan Palanivel',
    author_email='murugesan.p@payoda.com',
    license='Apache 2.0',
    install_requires=[],
)
