import json
import requests,logging,base64
logging = logging.getLogger(__name__)

class RightFaxWS(object):
    """
    RightFax Server WebService API class.
    Compatible with version 10.x
    """

    def __init__(self,webservice_url,auth_string=None):
        """
        Set client object
        params: Webservice URL,<session_id>
        """
        logging.info('Webservice URL: %s', webservice_url)
        try:
           self.base_url=webservice_url
           self.headers = {}
           if auth_string:
               self.auth_string = auth_string
               self.headers.update({'Authorization': 'Basic '+self.auth_string})
        except Exception, e:
            return self.error_handler(e)


    def authorize(self):
        logging.info('Initiate Login ...')
        logging.debug('Login details Username:%s', (base64.b64decode(self.auth_string)).split(":")[0])
        return self.api_call('GET')

    def send_attachments(self, attachment_details):
        """
        POST Attachments to RightFax
        :param attachment_details:
        :return:
        """
        logging.info('Initiate Sending Attachments ...')
        self.base_url = self.base_url + '/Attachments'
        files = {"file": (attachment_details.get('file_name', ''), open(attachment_details.get('file_path', ''), "rb"))}
        kwargs = {'files': files}
        logging.debug('URL: %s',self.base_url)
        logging.debug('Payload: %s',kwargs)
        return self.api_call('POST',kwargs)

    def send_new_fax(self,fax_details,sender,recipients,attachments):
        """
        Send Fax
        params: fax_details,sender,recipients, attachments
        return: response
        """
        try:
            logging.info('Sending New fax ...')
            sender_factory, recipient_factory, attachment_factory = {},[],[]

            if sender is not None:
                sender_factory = self.set_sender(sender)

            if recipients is not None:
                recipient_factory = self.set_recipient(recipients)

            if attachments is not None:
                attachment_factory =  self.frame_attachment_data(attachments)

            payload = {
                'Recipients' : recipient_factory,
                'Attachments' : attachment_factory,
                'Tags' : sender_factory
            }
            self.base_url = self.base_url + '/SendJobs'
            logging.debug('Fax details object: %s',fax_details)
            kwargs = {'data': json.dumps(payload)}
            self.headers.update({'Content-Type': 'application/json'})
            response = self.api_call('POST', kwargs)
            logging.debug('URL: %s', self.base_url)
            logging.debug('Payload: %s',json.dumps(payload))
            logging.debug('Send new fax response: %s',response)
            logging.info('Fax sent.')
            return response
        except Exception, e:
            return self.error_handler(e)

    def set_sender(self, sender_data):
        """
        Set Sender data
        param: sender data
        return: sender_obj
        """
        sender_data_obj = {}
        logging.info('Setting sender object ...')
        sender_data_obj['FromName'] = str(sender_data.get('fax_name', ''))
        sender_data_obj['FromFaxNumber'] = str(sender_data.get('fax_no', ''))
        logging.debug('Sender object: %s', sender_data_obj)
        return sender_data_obj

    def set_recipient(self,recipient_data):
        """
        Set Recipient data
        param: recipient data
        return: recipient_obj
        """
        logging.info('Setting recipient object ...')
        recipients = []
        for receipient in recipient_data:
            receipient_details = {}
            receipient_details['Name'] = str(receipient.get('name', ''))
            receipient_details['Destination'] = str(receipient.get('fax_no', ''))
            recipients.append(receipient_details)

        logging.debug('Recipients list: %s',recipients)
        return recipients

    def frame_attachment_data(self, attachment_data):
        """
        Frame Attachment data
        param: attachment_data
        return: attachment_obj
        """
        logging.info('Setting attachment object ...')
        attachments = []
        for attachment in attachment_data:
            attachement_details = {}
            attachement_details['AttachmentUrl'] = attachment
            attachments.append(attachement_details)
        logging.debug('Attachment details: %s',attachments)
        return attachments


    def check_fax_status(self,jobId):
        """
        Check the fax status
        :return:
        """
        try:
         logging.info("Checking the fax status....")
         self.base_url = self.base_url + '/Documents'
         kwargs = {'params': {'filter': 'job', 'jobid': jobId}}
         response = self.api_call('GET', kwargs)
         logging.debug('URL: %s', self.base_url)
         logging.debug('Queryparams : %s',str(kwargs))
         logging.debug('Check Status Rightfax response: %s', response)
         return response
        except Exception,e:
            return self.error_handler(e)

    def receive_new_fax(self, minTransmitTime, url_param = None):
        from urlparse import urlparse, parse_qsl
        try:
            logging.info('Receiving New fax ...')
            kwargs, params = {}, {}
            if not url_param:
                self.base_url = self.base_url + '/Documents'
                params['filter'] = 'received'
                params['minTransmitTime'] = minTransmitTime
            else:
                self.base_url = self.base_url +'/Documents'
                parse_url = urlparse(url_param)
                params = dict(parse_qsl(parse_url.query))
            kwargs['params'] = params
            response = self.api_call('GET', kwargs)
            logging.debug('URL: %s', self.base_url)
            logging.debug('Query Params: %s', json.dumps(params))
            logging.debug('Receive new fax response: %s', response)
            logging.info('Receive Fax done')
            return response
        except Exception, e:
            return self.error_handler(e)

    def get_fax_file_content(self, job_id):
        try:
            logging.info('Fetching file content of the job id : %s',job_id)
            self.base_url = self.base_url + '/DocumentFiles/' + job_id
            response = self.api_call('GET')
            logging.debug('URL: %s', self.base_url)
            logging.info('Fetching file content done')
            return response
        except Exception, e:
            return self.error_handler(e)

    def error_handler(self, err):
        """
        Error handler
        """
        error = {'status': False, 'details': err}
        logging.error('Exception occured due to : %s', error)
        return error

    def api_call(self,method,kwargs={}):
        try:
            res = requests.request(method, self.base_url,headers=self.headers,**kwargs)
            res.raise_for_status()
            return {'status':True,'details':res.content}
        except requests.exceptions.HTTPError as errh:
            return {"status":False,"details":"Http Error:" +str(errh)}
        except requests.exceptions.ConnectionError as errc:
            return {"status":False,"details":"Error Connecting:"+str(errc)}
        except requests.exceptions.Timeout as errt:
            return {"status":False,"details":"Timeout Error:"+str(errt)}
        except requests.exceptions.RequestException as err:
            return {"status":False,"details":"OOps: Something Else"+str(err)}


